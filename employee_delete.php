<?php

header('Content-Type: application/json');
require_once '_db.php';

$valid = false;

if (isset($_POST['id'])) {
	$valid = true;

	$id = $_POST['id'];
} else {
	$postData = json_decode(file_get_contents('php://input'));

	if (isset($postData->id)) {
		$valid = true;

		$id = $postData->id;
	}
}

if ($valid) {
	try {

		/*
		$db = new PDO($dsn, $username, $password);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$sql = "DELETE FROM `$employee` WHERE `id`=$id;";
		$statement = $db->prepare($sql);
		$statement->execute();

		$db = null;

		echo json_encode(array(
			'result' => "Employee # $id has been deleted",
			'sql' => $sql
		), JSON_NUMERIC_CHECK);
		*/

		$sql = "DELETE FROM `$employee` WHERE `id`=$id;";
		echo json_encode(array('SQL' => $sql), JSON_NUMERIC_CHECK);

	} catch(\PDOException $error) {

		die('No connections to the database<br/>' . $error);

	}
} else {
	echo json_encode(array(
		'Message' => 'Error: Input data is invalid'
	));
}
