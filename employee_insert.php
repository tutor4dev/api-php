<?php

header('Content-Type: application/json');
require_once '_db.php';

$valid = false;

if (isset($_POST['id'])) {
	$valid = true;

	$id = $_POST['id'];
	$first_name = $_POST['first_name'];
	$last_name = $_POST['last_name'];
} else {
	$postData = json_decode(file_get_contents('php://input'));

	if (isset($postData->id)) {
		$valid = true;

		$id = $postData->id;
		$first_name = $postData->first_name;
		$last_name = $postData->last_name;
	}
}

if ($valid) {
	try {

		/*
		$db = new PDO($dsn, $username, $password);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$sql = "INSERT INTO `$employee` (`id`, `first_name`, `last_name`) VALUES ($id, '$first_name', '$last_name');";
		$statement = $db->prepare($sql);
		$statement->execute();

		$db = null;

		echo json_encode(array(
			'result' => "Employee # $id has been inserted",
			'sql' => $sql
		), JSON_NUMERIC_CHECK);
		*/

		$sql = "INSERT INTO `$employee` (`id`, `first_name`, `last_name`) VALUES ($id, '$first_name', '$last_name');";
		echo json_encode(array('SQL' => $sql), JSON_NUMERIC_CHECK);

	} catch(PDOException $error) {

		die('No connections to the database<br/>' . $error);

	}
} else {
	echo json_encode(array(
		'Message' => 'Error: Input data is invalid'
	));
}
