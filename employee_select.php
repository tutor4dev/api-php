<?php

header('Content-Type: application/json');
require_once '_db.php';

try {

	$db = new PDO($dsn, $username, $password);

	if (isset($_GET['id'])) {

		$id = $_GET['id'];
		$sql = "SELECT * FROM `$employee` WHERE `id` = $id";

	} elseif (isset($_GET['first_name'])) {

		$first_name = $_GET['first_name'];
		$sql = "SELECT * FROM `$employee` WHERE `first_name` COLLATE UTF8_GENERAL_CI LIKE '%$first_name%' LIMIT $limit";

	} else {

		$sql = "SELECT * FROM `$employee` LIMIT $limit";

	}

	$statement = $db->prepare($sql);
	$statement->execute();

	$employees = $statement->fetchAll(PDO::FETCH_ASSOC);
	$db = null;

	echo json_encode($employees, JSON_NUMERIC_CHECK);

} catch(PDOException $error) {

	die('No connections to the database<br/>' . $error);

}