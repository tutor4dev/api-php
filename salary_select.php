<?php

header('Content-Type: application/json');
require_once '_db.php';

try {

	$db = new PDO($dsn, $username, $password);

	if (isset($_GET['employee_id'])) {

		$employee_id = $_GET['employee_id'];
		$sql = "SELECT * FROM `$salary` WHERE `employee_id` = $employee_id";

	} else {

		$sql = "SELECT * FROM `$salary` LIMIT $limit";

	}

	$statement = $db->prepare($sql);
	$statement->execute();

	$employees = $statement->fetchAll(PDO::FETCH_ASSOC);
	$db = null;

	echo json_encode($employees, JSON_NUMERIC_CHECK);

} catch(PDOException $error) {

	die('No connections to the database<br/>' . $error);

}